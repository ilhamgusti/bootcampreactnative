/**
 * Soal 1
 */

//release 0
class Animal {
    constructor(name){
        this.name = name || 'unnamed animal';
        this.legs = 4;
        this.cold_blooded = false
    }
   

}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false



//release 1
// Code class Ape dan class Frog di sini

class Ape extends Animal {
    constructor(name){
        super(name)
    }

    yell(){
        console.log("Auooo")
    }
}

class Frog extends Animal {
    constructor(name){
        super(name)
    }

    jump(){
        console.log("hop hop")
    }
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 




/**
 * Soal 2
 */
//ini contoh dalam function
function Clock({ template }) {
  
  var timer;

  function render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  this.stop = function() {
    clearInterval(timer);
  };

  this.start = function() {
    render();
    timer = setInterval(render, 1000);
  };

}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 



// ubah menjadi class
class Clock {
    constructor({ template = 'h:m:s' }) {
        this.timer;
        this.output = template;
        this.date = new Date();
        this.hours = this.date.getHours();
        this.mins = this.date.getMinutes();
        this.secs = this.date.getSeconds();
    }
    // Code di sini
    render() {
        if (this.hours < 10) this.hours = '0' + this.hours;
        if (this.mins < 10) this.mins = '0' + this.mins;
        if (this.secs < 10) this.secs = '0' + this.secs;
        this.output = this.output.replace('h', this.hours).replace('m', this.mins).replace('s', this.secs);
        console.log(this.output);
    }
    stop() {
      clearInterval(this.timer);
    }
    start() {
        this.render();
        this.timer = setInterval(()=>this.render(), 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 
