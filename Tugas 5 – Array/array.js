/**
 * Soal 1
 * menggunakan recursive
 */
function range(startNum, finishNum) {
  if (!startNum || !finishNum) {
    return -1;
  }
  if (startNum === finishNum) return [startNum];

  if (startNum > finishNum) {
    return [startNum, ...range(startNum - 1, finishNum)];
  }
  if (startNum < finishNum) {
    return [startNum, ...range(startNum + 1, finishNum)];
  }
}

console.log(range(5, 1));

/**
 * Soal 2
 */
function rangeWithStep(startNum, finishNum, stepper) {
  var answer = [];
  //validasi jika parameter kosong
  if (!startNum || !finishNum) {
    return -1;
  } else if (startNum === finishNum) {
    answer.push(startNum);
  } else if (!stepper) {
    return range(startNum, finishNum);
  } else if (startNum > finishNum) {
    for (var i = startNum; i >= finishNum; i -= stepper) {
      answer.push(i);
    }
  } else if (startNum < finishNum) {
    for (var i = startNum; i <= finishNum; i += stepper) {
      answer.push(i);
    }
  }

  return answer;
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]


/**
 *  Soal 3
 * @param {number} startNum nilai dimulai dari angka berapa
 * @param {number} finishNum akhir arraynya berapa
 * @param {number} stepper berapa step range
 */
function sum(startNum, finishNum, stepper) {
  var resultSum = 0;
  var hasilArray = rangeWithStep(startNum, finishNum, stepper);

  if (hasilArray === -1) {
    return startNum || 0;
  }

  if (Array.isArray(hasilArray)) {
    for (var n of hasilArray) resultSum += n;
    return resultSum;
  }
}

console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0
