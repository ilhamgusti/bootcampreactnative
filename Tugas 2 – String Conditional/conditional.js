
/**
 * if else
 */
var nama = "Ilham";
var peran = "Werewolf";

if (!!nama === false) {
  console.log("Nama harus di isi");
}else if(peran.toLowerCase() === 'werewolf'){
  console.log("Hallo "+peran +" "+ nama + ", Kamu akan memakan mangsa setiap malam!" )
}else if(peran.toLowerCase() === 'guard'){
  console.log("Hallo "+peran +" "+ nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf." )
}else if(peran.toLowerCase() === 'penyihir'){
  console.log("Hallo "+peran +" "+ nama + ", kamu dapat melihat siapa yang menjadi werewolf!" )
}else if(!!peran === false){
  console.log("Hallo " + nama + ", Pilih peranmu untuk memulai game!");
}else{
  console.log("Hallo " + nama + ", "+peran+" bukan termasuk dalam peran, pilihan peran hanya 'Werewolf', 'Guard', dan 'Penyihir'.");
}


/**
 * Switch Case
 */
var tanggal = 1;
var bulan = 5;
var tahun = 1996;
var hasil;

var listBulan = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "November",
  "Desember"
];

switch (true) {
  case bulan === 1:
    hasil = tanggal + " " + listBulan[bulan - 1] + " " + tahun;
    break;
  case bulan === 2:
    hasil = tanggal + " " + listBulan[bulan - 1] + " " + tahun;
    break;
  case bulan === 3:
    hasil = tanggal + " " + listBulan[bulan - 1] + " " + tahun;
    break;
  case bulan === 4:
    hasil = tanggal + " " + listBulan[bulan - 1] + " " + tahun;
    break;
  case bulan === 5:
    hasil = tanggal + " " + listBulan[bulan - 1] + " " + tahun;
    break;
  case bulan === 6:
    hasil = tanggal + " " + listBulan[bulan - 1] + " " + tahun;
    break;
  case bulan === 7:
    hasil = tanggal + " " + listBulan[bulan - 1] + " " + tahun;
    break;
  case bulan === 8:
    hasil = tanggal + " " + listBulan[bulan - 1] + " " + tahun;
    break;
  case bulan === 9:
    hasil = tanggal + " " + listBulan[bulan - 1] + " " + tahun;
    break;
  case bulan === 10:
    hasil = tanggal + " " + listBulan[bulan - 1] + " " + tahun;
    break;
  case bulan === 11:
    hasil = tanggal + " " + listBulan[bulan - 1] + " " + tahun;
    break;
  case bulan === 12:
    hasil = tanggal + " " + listBulan[bulan - 1] + " " + tahun;
    break;
  default:
    hasil = "out of range";
}

console.log(hasil);


