var readBooksPromise = require("./promise.js");
var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 }
];

const waktunyaMembaca = async (books, time, index = 0) => {
  if (!books[index]?.timeSpent) {
    console.log("Waktu anda membaca tidak cukup untuk membaca hal yang lain");
  } else {
    const sisa = await readBooksPromise(time, books[index]);
    index = index + 1;
    waktunyaMembaca(books, sisa, index);
  }
};

waktunyaMembaca(books, 10000);
