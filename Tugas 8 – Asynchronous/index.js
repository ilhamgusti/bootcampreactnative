var readBooks = require('./callback.js')
var books = [
    {name:'LOTR', timeSpent:3000},
    {name:'Fidas', timeSpent:2000},
    {name:'Kalkulus', timeSpent:4000},

]

const waktunyaMembaca = (books, time, index = 0) => {
  if (!books[index]?.timeSpent) {
    console.log("Waktu anda membaca tidak cukup untuk membaca hal yang lain");
  } else {
    readBooks(time, books[index], (sisa) => {
      return waktunyaMembaca(books, sisa, index);
    });
    index = index + 1;
  }
};

waktunyaMembaca(books, 10000);
