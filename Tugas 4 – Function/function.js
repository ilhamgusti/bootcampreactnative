
/**
 * Soal 1
*/
function teriak(){
    return "Halo Sanbers!";
}
 
console.log(teriak()); // "Halo Sanbers!" 


/**
 *  Soal 2
 */
function kalikan(a,b){
    return a * b;
}
var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali); // 48


/**
 * Soal 3
 */

function introduce(name, age, address, hobby){
    if(!(name || age || address || hobby)){
        return "Harap isikan semua data pada parameter!!";
    }
    
    return "Nama saya "+name+", umur saya "+age+" tahun, alamat saya "+address+", dan saya punya hobby yaitu "+hobby+"!" ;
}
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 
