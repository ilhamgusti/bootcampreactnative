/**
 * SOAL 1
 */
function arrayToObject(arr) {
  if (!arr.length > 0) {
    return "";
  }
  return arr.map(function ([firstName, lastName, gender, birthDate], index) {
    const fullName = firstName + " " + lastName;
    const thisYear = new Date().getFullYear();
    let age;
    if (!birthDate || birthDate > thisYear) {
      age = "Invalid Birth Year";
    } else {
      age = thisYear - birthDate;
    }
    const resultObj = {
      [fullName]: {
        firstName,
        lastName,
        gender,
        age
      }
    };
    return (
      index +
      1 +
      ". " +
      JSON.stringify(resultObj, null, 2)
        .replace(/(^[{])/g,"")
        .replace(/"([^"]+)":/g, "$1:")
    );
  });
}

// Driver Code
var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"]
];
console.log(arrayToObject(people).join());
/*
1. Bruce Banner: { 
      firstName: "Bruce",
      lastName: "Banner",
      gender: "male",
      age: 45
  }
  2. Natasha Romanoff: { 
      firstName: "Natasha",
      lastName: "Romanoff",
      gender: "female".
      age: "Invalid Birth Year"
    }
    */

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023]
];
arrayToObject(people2);
console.log(arrayToObject(people2).join());
/*
1. Tony Stark: { 
      firstName: "Tony",
      lastName: "Stark",
      gender: "male",
      age: 40
    }
  2. Pepper Pots: { 
    firstName: "Pepper",
      lastName: "Pots",
      gender: "female".
      age: "Invalid Birth Year"
    }
*/

// Error case
// arrayToObject([]); // ""

// console.log(arrayToObject(people2));




/**
 * SOAL 2
 */
function shoppingTime(memberId, money) {
  // you can only write your code here!
  var listPurchased = {
    "Sepatu Stacattu": 1500000,
    "Baju Zoro": 500000,
    "Baju H&N": 250000,
    "Sweater Uniklooh": 175000,
    "Casing Handphone": 50000
  };
  let totalPurchased1;
  switch (money) {
    case money > listPurchased["Sepatu Stacattu"]:
      var changeMoney = money - totalPurchased1;
      console.log(changeMoney);
      totalPurchased1 = listPurchased["Sepatu Stacattu"];
      break;
    case money > listPurchased["Baju Zoro"]:
      totalPurchased1 = listPurchased["Baju Zoro"];
      break;
    case money > listPurchased["Baju H&N"]:
      totalPurchased1 = listPurchased["Baju H&N"];
      break;
    case money > listPurchased["Sweater Uniklooh"]:
      totalPurchased1 = listPurchased["Sweater Uniklooh"];
      break;
    case money > listPurchased["Casing Handphone"]:
      totalPurchased1 = listPurchased["Casing Handphone"];
      break;
    default:
      totalPurchased1 = 0;
      break;
  }
  console.log(totalPurchased1);
  var changeMoney = money - totalPurchased1;

  if (!memberId || !money) {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else if (money <= 50000) {
    return "Mohon maaf, uang tidak cukup";
  } else {
    return (
      "memberId: " +
      memberId +
      ", \n money: " +
      money +
      ", \n listPurchased: " +
      [listPurchased] +
      ", \n changeMoney" +
      changeMoney
    );
  }
}

// TEST CASES
console.log(shoppingTime("1820RzKrnWn08", 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime("82Ku8Ma742", 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

function naikAngkot(arrPenumpang) {
  var hasil = [],
    bayar = 0;
  rute = ["A", "B", "C", "D", "E", "F"];
  for (var i = 0; i < arrPenumpang.length; i++) {
    for (var j = 0; j < rute.length; j++) {
      if (rute[j] == arrPenumpang[i][1]) {
        for (var k = j; k < rute.length; k++) {
          if (rute[k] == arrPenumpang[i][2]) {
            break;
          } else {
            bayar += 2000;
          }
        }
        hasil.push(
          "{ penumpang: '" +
            arrPenumpang[i][0] +
            "', naikDari: '" +
            arrPenumpang[i][1] +
            "', tujuan: '" +
            arrPenumpang[i][2] +
            "', bayar: " +
            bayar +
            " }"
        );
      } else {
        bayar = 0;
        continue;
      }
    }
  }

  return hasil;
}

console.log(arrayToObject([]));
